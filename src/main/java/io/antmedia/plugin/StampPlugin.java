package io.antmedia.plugin;

import io.antmedia.AntMediaApplicationAdapter;
import io.antmedia.app.StampFrameListener;
import io.antmedia.plugin.api.IStreamListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component(value="plugin.stamp")
public class StampPlugin implements ApplicationContextAware, IStreamListener {

	private static Logger logger = LoggerFactory.getLogger(StampPlugin.class);
	
	private ApplicationContext applicationContext;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		AntMediaApplicationAdapter app = getApplication();
		logger.info("Overlay Issue Plugin is starting in {}", app.getName());
		initComponents(app);
	}

	private void initComponents(AntMediaApplicationAdapter app) {
		initialize(app);
	}

	private void initialize(AntMediaApplicationAdapter app) {
		app.addStreamListener(this);

	}

	public AntMediaApplicationAdapter getApplication() {
		return (AntMediaApplicationAdapter) applicationContext.getBean(AntMediaApplicationAdapter.BEAN_NAME);
	}
	
	@Override
	public void streamStarted(String streamId) {
		logger.info("Registering frame listener in stream {}", streamId);
		StampFrameListener frameListener = new StampFrameListener();
		AntMediaApplicationAdapter app = getApplication();
		app.addFrameListener(streamId, frameListener);
	}

	@Override
	public void streamFinished(String streamId) {
		// TODO https://community.antmedia.io/topic/1515-do-frame-listeners-need-to-be-unregistered/
		AntMediaApplicationAdapter app = getApplication();
		for(var L : app.getListeners()) {
			logger.info("Found listener L " + L + " when stopping stream {}", streamId);
		}
	}

	@Override
	public void joinedTheRoom(String roomId, String streamId) {
	}

	@Override
	public void leftTheRoom(String roomId, String streamId) {
	}

}