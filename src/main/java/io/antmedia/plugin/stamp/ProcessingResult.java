package io.antmedia.plugin.stamp;

import java.awt.image.BufferedImage;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class ProcessingResult {

    private final List<BufferedImageWithCoord> bufferedImages = new CopyOnWriteArrayList<>();

    private boolean imageChanged;

    public void setImageChanged() {
        this.imageChanged = true;
    }

    public boolean isImageChanged() {
        return imageChanged;
    }

    public void addOverlay(BufferedImage bufferedImage, int x, int y) {
        this.bufferedImages.add(new BufferedImageWithCoord(bufferedImage, x, y));
    }

    public void appyResult(BufferedImage originalImage) {
        for(BufferedImageWithCoord b : bufferedImages) {
            boolean drawn = originalImage.getGraphics().drawImage(b.getImage(), b.getX(), b.getY(), null);
        }
    }

    static class BufferedImageWithCoord {

        private final BufferedImage image;

        private final int x, y;

        public BufferedImageWithCoord(BufferedImage image, int x, int y) {
            this.image = image;
            this.y = y;
            this.x = x;
        }

        public BufferedImage getImage() {
            return image;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
