package io.antmedia.plugin.stamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class StampEngine {

    public StampEngine(String streamName) {
    }


    public ProcessingResult processFrame(long frameCount) {
        ProcessingResult result = new ProcessingResult();

        BufferedImage bi = new BufferedImage(300, 150, BufferedImage.TYPE_4BYTE_ABGR);
        bi.getGraphics().setColor(Color.white);
        bi.getGraphics().setFont(Font.getFont("SansSerif"));

        bi.getGraphics().drawString("Stamp " + frameCount, 10, 10);

        result.addOverlay(bi, 1, 1);
        result.setImageChanged();

        return result;
    }

}
