package io.antmedia.plugin.stamp;

import org.bytedeco.ffmpeg.avutil.AVFrame;
import org.bytedeco.ffmpeg.swscale.SwsContext;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.DoublePointer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static org.bytedeco.ffmpeg.global.avutil.AV_PIX_FMT_RGBA;
import static org.bytedeco.ffmpeg.global.avutil.av_image_fill_arrays;
import static org.bytedeco.ffmpeg.global.avutil.av_image_get_buffer_size;
import static org.bytedeco.ffmpeg.global.avutil.av_malloc;
import static org.bytedeco.ffmpeg.global.swscale.SWS_BICUBIC;
import static org.bytedeco.ffmpeg.global.swscale.sws_freeContext;
import static org.bytedeco.ffmpeg.global.swscale.sws_getCachedContext;
import static org.bytedeco.ffmpeg.global.swscale.sws_scale;


public class Utils {

	private static BytePointer targetFrameBuffer;

	private static BytePointer rgbFrameBuffer;

	public static void saveRGB(BufferedImage image, String fileName) {
		try {
			ImageIO.write(image, "jpeg", new File(fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	public static AVFrame toRGBA(AVFrame inFrame, AVFramePool pool) {
		int format = AV_PIX_FMT_RGBA; // if set to AV_PIX_FMT_RGBA;
		//AV_PIX_FMT_ABGR

		SwsContext sws_ctx = null;
		sws_ctx = sws_getCachedContext(sws_ctx, inFrame.width(), inFrame.height(), inFrame.format(),
				inFrame.width(), inFrame.height(), format,
				SWS_BICUBIC, null, null, (DoublePointer)null);


		AVFrame outFrame = pool.getAVFrame();
		int size = av_image_get_buffer_size(format, inFrame.width(), inFrame.height(), 32);
		if(rgbFrameBuffer == null) {
			rgbFrameBuffer = new BytePointer(av_malloc(size)).capacity(size);
		}
		
		av_image_fill_arrays(outFrame.data(), outFrame.linesize(), rgbFrameBuffer, format, inFrame.width(), inFrame.height(), 32);
		outFrame.format(format);
		outFrame.width(inFrame.width());
		outFrame.height(inFrame.height());

		sws_scale(sws_ctx, inFrame.data(), inFrame.linesize(),
				0, inFrame.height(), outFrame.data(), outFrame.linesize());

		outFrame.pts(inFrame.pts());
		
		
		sws_freeContext(sws_ctx);
		sws_ctx.close();
		
		return outFrame;
	}


	public static byte[] getRGBData(BufferedImage image) {
		
		int width = image.getWidth();
		int height = image.getHeight();
		
		byte data[] = new byte[width*height*4];
		int k = 0;
		for(int y = 0; y < height; y++) {
			for(int x = 0; x < width; x++) {
				Color c = new Color(image.getRGB(x, y));
				data[k++] = (byte)c.getRed();
				data[k++] = (byte)c.getGreen();
				data[k++] = (byte)c.getBlue();
				data[k++] = (byte)c.getAlpha();
			}
		}
		
		return data;
	}


	public static AVFrame toTargetFormat(AVFrame inFrame, int format, AVFramePool pool) {
		SwsContext sws_ctx = null;
		sws_ctx = sws_getCachedContext(sws_ctx, inFrame.width(), inFrame.height(), inFrame.format(),
				inFrame.width(), inFrame.height(), format,
				SWS_BICUBIC, null, null, (DoublePointer)null);


		AVFrame outFrame = pool.getAVFrame();
		int size = av_image_get_buffer_size(format, inFrame.width(), inFrame.height(), 32);
		if(targetFrameBuffer == null) {
			targetFrameBuffer = new BytePointer(av_malloc(size)).capacity(size);
		}
		
		av_image_fill_arrays(outFrame.data(), outFrame.linesize(), targetFrameBuffer, format, inFrame.width(), inFrame.height(), 32);
		outFrame.format(format);
		outFrame.width(inFrame.width());
		outFrame.height(inFrame.height());

		sws_scale(sws_ctx, inFrame.data(), inFrame.linesize(),
				0, inFrame.height(), outFrame.data(), outFrame.linesize());

		outFrame.pts(inFrame.pts());
		
		sws_freeContext(sws_ctx);
		sws_ctx.close();
		
		return outFrame;
	}
	
}

