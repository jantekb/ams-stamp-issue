package io.antmedia.app;

import io.antmedia.plugin.StampPlugin;
import io.antmedia.plugin.api.IFrameListener;
import io.antmedia.plugin.api.StreamParametersInfo;
import io.antmedia.plugin.stamp.AVFramePool;
import io.antmedia.plugin.stamp.ProcessingResult;
import io.antmedia.plugin.stamp.StampEngine;
import io.antmedia.plugin.stamp.Utils;
import org.bytedeco.ffmpeg.avutil.AVFrame;
import org.bytedeco.javacpp.BytePointer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;

import static org.bytedeco.ffmpeg.global.avutil.av_frame_ref;

public class StampFrameListener implements IFrameListener {

	private static Logger logger = LoggerFactory.getLogger(StampPlugin.class);

	private long videoFrameCount = 0;

	private AVFrame yuvFrame;

	private AVFramePool framePool;

	private StampEngine stampEngine;

	public StampFrameListener() {
		this.framePool = new AVFramePool();
	}

	@Override
	public AVFrame onAudioFrame(String streamId, AVFrame audioFrame) {
		return audioFrame;
	}

	@Override
	public AVFrame onVideoFrame(String streamId, AVFrame videoFrame) {
		if(stampEngine == null) {
			stampEngine = new StampEngine(streamId);
		}
		videoFrameCount++;

		StopWatch stopWatch = new StopWatch("onVideoFrame");

		stopWatch.start("overlay calculation");
		ProcessingResult result = stampEngine.processFrame(videoFrameCount);
		stopWatch.stop();

		if(!result.isImageChanged()) {
			logger.debug("No image manipulation required on stream {}", streamId);
			return videoFrame;
		} else {
			stopWatch.start("yuv2rgb");

			// since we return yuvFrame, we need to release it in the next call
			if(yuvFrame != null) {
				framePool.addFrame2Pool(yuvFrame);
			}

			int format = videoFrame.format();

			AVFrame cloneframe = framePool.getAVFrame();
			av_frame_ref(cloneframe, videoFrame);

			AVFrame rgbaFrame = Utils.toRGBA(cloneframe, framePool);
			framePool.addFrame2Pool(cloneframe);

			stopWatch.stop();

			byte[] rgbaData = new byte[rgbaFrame.width() * rgbaFrame.height() * 4];

			stopWatch.start("frame data copy");
			rgbaFrame.data(0).get(rgbaData);
			stopWatch.stop();

			stopWatch.start("toBufferedImage");
			final BufferedImage bufferedImage = toBufferedImageRgba(rgbaFrame.width(), rgbaFrame.height(), rgbaData);
			stopWatch.stop();

			result.appyResult(bufferedImage);

			stopWatch.start("rgb2yuv");
			rgbaFrame.data(0, new BytePointer(rgbaData));
			yuvFrame = Utils.toTargetFormat(rgbaFrame, format, framePool);
			framePool.addFrame2Pool(rgbaFrame);
			stopWatch.stop();

			if(videoFrameCount % 400 == 0) {
				logger.debug(stopWatch.prettyPrint());
				logger.info("Frame pool size: {}", framePool.getSize());

			}
			return yuvFrame;
		}

	}

	BufferedImage toBufferedImageRgba(int width, int height, byte[] abgrData) {
		DataBuffer dataBuffer = new DataBufferByte(abgrData, width * height * 4, 0);
		ColorModel colorModel = new ComponentColorModel(ColorSpace.getInstance(ColorSpace.CS_sRGB),
				new int[] {8,8,8,8}, true, false, Transparency.OPAQUE, DataBuffer.TYPE_BYTE);
		WritableRaster raster = Raster.createInterleavedRaster(
				dataBuffer, width, height, width * 4, 4,
				new int[] {0, 1, 2, 3}, // AV_PIX_FMT_RGBA
				null);
		BufferedImage image = new BufferedImage(colorModel, raster, false, null);
		return image;
	}

	@Override
	public void writeTrailer(String streamId) {
	}

	@Override
	public void setVideoStreamInfo(String streamId, StreamParametersInfo videoStreamInfo) {
	}

	@Override
	public void setAudioStreamInfo(String streamId, StreamParametersInfo audioStreamInfo) {
	}

	@Override
	public void start() {
	}


}