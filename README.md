# ams-overlay-issue

Demonstration of two bugs related to overlays:

- two concurrent streams interfere and frames from one appear briefly on the other one
- overlays don't appear on the video pushed to YouTube with the AMS RTMP endpoint feature